import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimeListComponent } from './components/anime-list/anime-list.component';
import { AnimeCardComponent } from './components/anime-card/anime-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [AnimeListComponent, AnimeCardComponent],
  imports: [CommonModule, MatCardModule, MatButtonModule],
  exports: [AnimeListComponent],
})
export class SharedModule {}
