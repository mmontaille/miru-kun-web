import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-anime-card',
  templateUrl: './anime-card.component.html',
  styleUrls: ['./anime-card.component.scss'],
})
export class AnimeCardComponent implements OnInit {
  @Input() anime: any;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  /**
   * Navigates to details page
   */
  navigateToDetails(): void {
    this.router.navigateByUrl(`/anime/${this.anime.media.id}`);
  }
}
