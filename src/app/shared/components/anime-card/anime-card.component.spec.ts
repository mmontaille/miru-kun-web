import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { RouterTestingModule } from '@angular/router/testing';

import { AnimeCardComponent } from './anime-card.component';

describe('AnimeCardComponent', () => {
  let component: AnimeCardComponent;
  let fixture: ComponentFixture<AnimeCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnimeCardComponent],
      imports: [MatCardModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimeCardComponent);
    component = fixture.componentInstance;

    component.anime = {
      media: {
        id: 1,
        title: {
          romaji: 'Kimi no na wa',
        },
        seasonYear: 2019,
        progress: 0,
        episodes: 1,
        coverImage: {
          large: 'url',
        },
      },
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
