import { Component, Input, OnInit } from '@angular/core';
import { ListData } from 'src/app/core/models/list-data/list-data';

@Component({
  selector: 'app-anime-list',
  templateUrl: './anime-list.component.html',
  styleUrls: ['./anime-list.component.scss'],
})
export class AnimeListComponent implements OnInit {
  @Input() data!: ListData;

  constructor() {}

  ngOnInit(): void {}
}
