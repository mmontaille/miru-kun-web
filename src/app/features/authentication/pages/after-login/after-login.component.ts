import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.scss'],
})
export class AfterLoginComponent implements OnInit {
  constructor(
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Redirect to home if already logged in
    if (this.authenticationService.isLoggedIn()) {
      this.router.navigateByUrl('');
    }

    // Get fragment from url to store the token
    const fragment = this.route.snapshot.fragment;
    fragment?.split('&').forEach((elem) => {
      const elemArray = elem.split('=');
      localStorage.setItem(elemArray[0], elemArray[1]);
    });
    this.router.navigateByUrl('');
  }
}
