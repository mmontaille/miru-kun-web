import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ListData } from 'src/app/core/models/list-data/list-data';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  inProgressAnime$: Observable<ListData>;

  constructor(private apiService: ApiService) {
    this.inProgressAnime$ = this.apiService.getUserAnimeListCurrent().pipe(
      map((response) => ({
        title: 'Anime in Progress',
        animes: response,
      }))
    );
  }

  ngOnInit(): void {}
}
