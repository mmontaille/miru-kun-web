import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details/details.component';
import { AnimeRoutingModule } from './anime-routing.module';

@NgModule({
  declarations: [DetailsComponent],
  imports: [CommonModule, AnimeRoutingModule],
})
export class AnimeModule {}
