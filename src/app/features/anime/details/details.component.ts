import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit, OnDestroy {
  anime: any;

  private animeId!: number;

  private subs: Subscription[] = [];

  constructor(private router: ActivatedRoute, private apiService: ApiService) {
    this.loadAnimeId();
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  /**
   * Loads anime id from url
   */
  private loadAnimeId(): void {
    this.subs.push(
      // eslint-disable-next-line import/no-deprecated
      this.router.params.subscribe((params) => {
        this.animeId = params.id;
        this.loadAnime(this.animeId);
      })
    );
  }

  /**
   * Loads the corresponding id
   *
   * @param id id of the anime
   */
  private loadAnime(id: number): void {
    this.subs.push(
      // eslint-disable-next-line import/no-deprecated
      this.apiService.getAnime(id).subscribe((anime) => {
        this.anime = anime;
      })
    );
  }
}
