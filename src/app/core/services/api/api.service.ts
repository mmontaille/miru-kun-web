import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { JwtService } from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiRestUrl = environment.apiRestUrl;
  private userId: number;

  constructor(private http: HttpClient, private jwtService: JwtService) {
    this.userId = this.jwtService.getUserId();
  }

  getAnime(id: number): Observable<any> {
    const query = `query ($userId: Int, $id: Int) {
      MediaList (userId: $userId, mediaId: $id) {
        progress
        media {
          id
          title {
            romaji
          }
          bannerImage
        }
      }
    }`;

    const variables = {
      userId: this.userId,
      id,
    };

    return this.makeRequest(query, variables).pipe(
      map((data) => data.data.MediaList)
    );
  }

  /**
   * Gets user list of animes in progress
   */
  getUserAnimeListCurrent(): Observable<any> {
    const query = `query ($userId: Int, $type: MediaType, $status: MediaListStatus, $sort: [MediaListSort]) {
      MediaListCollection (userId: $userId, type: $type, status: $status, sort: $sort) {
        lists {
          entries {
            progress
            media {
              id
              episodes
              title {
                romaji
              }
              coverImage {
                large
              }
              seasonYear
            }
          }
        }
      }
    }`;

    const variables = {
      userId: this.userId,
      type: 'ANIME',
      status: 'CURRENT',
      sort: ['MEDIA_TITLE_ROMAJI'],
    };

    return this.makeRequest(query, variables).pipe(
      map((res) =>
        res.data.MediaListCollection.lists
          .map((list: any) => list.entries)
          .flat()
          .sort((a: any, b: any) =>
            a.media.title.romaji > b.media.title.romaji ? 1 : -1
          )
      )
    );
  }

  getCurrentSeasonAnimeList(): Observable<any> {
    const query = `query ($season: MediaSeason, $type: MediaType, $format: MediaFormat, $year: Int, $page: Int) {
      Page (page: $page) {
        pageInfo {
          total
          currentPage
          lastPage
          hasNextPage
          perPage
        }
        media (season: $season, seasonYear: $year, type: $type, format: $format) {
          id
          title {
            romaji
          }
          coverImage {
            large
          }
        }
      }
    }`;

    const variables = {
      season: 'WINTER',
      year: 2021,
      page: 1,
      type: 'ANIME',
      format: 'TV',
    };

    return this.makeRequest(query, variables);
  }

  getUserAnimeList(): Observable<any> {
    const query = `query ($userId: Int, $page: Int, $perPage: Int, $sort: [MediaListSort]) {
      Page (page: $page, perPage: $perPage) {
        pageInfo {
          total
          currentPage
          lastPage
          hasNextPage
          perPage
        }
        mediaList (userId: $userId, sort: $sort) {
          media {
            title {
              romaji
            }
            coverImage {
              large
            }
          }
        }
      }
    }`;

    const variables = {
      userId: this.userId,
      page: 1,
      perPage: 10,
      sort: ['MEDIA_TITLE_ROMAJI'],
    };

    return this.makeRequest(query, variables);
  }

  /**
   * Prepares the request for the anilist api
   *
   * @param query graphql query
   * @param variables variables to replace in the query
   */
  private makeRequest(query: string, variables?: any): Observable<any> {
    const settings = {
      headers: new HttpHeaders({
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Content-Type': 'application/json',
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Accept: 'application/json',
      }),
    };

    return this.http.post(
      this.apiRestUrl,
      JSON.stringify({
        query,
        variables,
      }),
      settings
    );
  }
}
