import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  private jwtHelperService: JwtHelperService;

  constructor() {
    this.jwtHelperService = new JwtHelperService();
  }

  /**
   * Returns user id
   */
  getUserId(): number {
    const token = localStorage.getItem('access_token')?.toString();
    const decodedToken = this.jwtHelperService.decodeToken(token);
    return +decodedToken?.sub;
  }
}
