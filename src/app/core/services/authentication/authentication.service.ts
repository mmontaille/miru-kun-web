import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  /**
   * Returns the login state
   */
  isLoggedIn(): boolean {
    return localStorage.getItem('access_token') ? true : false;
  }
}
