export interface NavigationButton {
  icon: string;
  isActive: boolean;
  url: string;
}
