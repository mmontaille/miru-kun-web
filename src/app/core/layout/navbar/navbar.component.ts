import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationButton } from '../../models/navigationButton/navigation-button';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  // Array of navigation buttons
  navigationArray: NavigationButton[] = [
    {
      icon: 'home',
      isActive: true,
      url: '',
    },
    {
      icon: 'library_books',
      isActive: false,
      url: 'list',
    },
    {
      icon: 'search',
      isActive: false,
      url: 'search',
    },
    {
      icon: 'person',
      isActive: false,
      url: 'profile',
    },
    {
      icon: 'more_horiz',
      isActive: false,
      url: 'menu',
    },
  ];

  constructor(private router: Router) {}

  ngOnInit(): void {}

  /**
   * Toggle active button
   *
   * @param button clicked button
   */
  toggleActiveButton(button: NavigationButton): void {
    // Get active button to change its state
    const activeButton = this.navigationArray.find(
      (x: NavigationButton) => x.isActive
    );
    if (activeButton) {
      activeButton.isActive = false;
    }

    // Change clicked button state
    button.isActive = true;

    this.router.navigateByUrl(button.url);
  }
}
